# Warp & WarpSim

Warp – High throughput, Low latency Message Bus
WarpSim - UDP client side simulator 

## Binaries
Linux, Windows and MacOS binaries for both Warp and WarpSim

## Docker - Standalone
### Warp
```
docker run -it --rm -p 127.0.0.1:8080:8080  -p 127.0.0.1:40000:40000/udp -p 127.0.0.1:40001:40001/udp -p 127.0.0.1:40002:40002/udp -p 127.0.0.1:40003:40003/udp  -p 127.0.0.1:40004:40004/udp  -p 127.0.0.1:40005:40005/udp  -p 127.0.0.1:40006:40006/udp -p 127.0.0.1:40007:40007/udp tradency/warp:latest
```
### WarpSim
```
docker run -it --rm -p 127.0.0.1:8090:8080  tradency/warpsim:latest
```

## Docker - Compose
Docker compose for single and cluster modes, ymls provided

